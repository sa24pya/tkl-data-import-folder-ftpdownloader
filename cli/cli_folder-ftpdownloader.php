<?php
/**
 * cli/cli_folder-ftpdownloader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */

// Directory Processor
use DataImportFTPDownloader\FTPDownloader;

// .env
use Dotenv\Dotenv;
// Log
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    fwrite(STDERR,
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}

$dotenv = new \Dotenv\Dotenv( dirname( __DIR__ ));
$dotenv->load();

// Log
$log = new Logger('data-import-folder-ftpdownloader');
$log->pushHandler(new StreamHandler(getenv('PATH_TO_LOG_FILE'), Logger::INFO));
$log->info('Called..');


$ftpd = new FTPDownloader ();


try { 
    $log->info('Called..');
    fwrite(STDERR,"Downloading to " . getcwd()  . getenv('FTPLOCALFOLDER') .  "\n");
    $ftpd->download(
        [
            'ftpUser'         => getenv('FTPUSER'),
            'ftpPassword'     => getenv('FTPPASSWORD'),
            'ftpServer'       => getenv('FTPSERVER'),
            'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'),
            'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'),
	    'ftpRemoteAfterFolder' => getenv('FTPREMOTEAFTERFOLDER'),
            'move'            => getenv('MOVE'),

        ]
    );

    $log->info("Downloaded into: " . getenv('FTPLOCALFOLDER'));
}catch( Exception $e) {
    // write message to the log file
    $log->error(var_export($e, true));
    fwrite(STDERR,$e->getMessage() . "\n");
}
$log->info("Done.");
