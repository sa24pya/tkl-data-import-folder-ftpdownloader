<?php
namespace DataImportFTPDownloader\tests;

/**
 * File: FTPDownloaderTest.php
 *
 * @author mrx, Dmitriy, Etor
 *
 * @package    DataImportFTPDownloader
 * @subpackage FTPDownloader
 * @version    1.0.3
 */
use DataImportFTPDownloader\FTPDownloader;
use Dotenv\Dotenv;

/**
 * Class FTPDownloader
 *
 * @package:   DataImportFTPDownloader
 * @subpackage FTPDownloader
 * @version:   1.0.3
 */
class FTPDownloaderTest extends \PHPUnit_Framework_TestCase
{
    private $_ftp;
    private $fileNames = [];
    private $ftpConnection = null;

    public function setUp()
    {
        $this->_ftp = new FTPDownloader();
        $dotenv = new Dotenv(__DIR__);
        $dotenv->load();
        $this->fileNames = [];
        $this->uploadTestFiles(5);
    }

    public function tearDown()
    {
        $connection = $this->getConnection();
        $ds = \DIRECTORY_SEPARATOR;
        foreach ($this->fileNames as $fileName) {
            try {
                ftp_delete($connection, '/'.getenv('FTPREMOTEFOLDER') . $ds . $fileName);
            } catch (\Exception $e) {
                try {
                    ftp_delete($connection, '/'.getenv('FTPREMOTEAFTERFOLDER') . $ds . $fileName);
                } catch (\Exception $e) {
                    //the file couldn't be found!
                }
            }
        }
    }
    /**
     * getConnection() : Obtains an ftp connection. If it doesn't exists a new one is created.
     * @return ftp_conn
     */
    protected function getConnection()
    {
        if (!$this->ftpConnection) {
            $conn_id = ftp_ssl_connect(getenv('FTPSERVER')) or die("Can't connect " . getenv('FTPSERVER'));
            ftp_login($conn_id, getenv('FTPUSER'), getenv('FTPPASSWORD'));
            ftp_pasv($conn_id, true);
            $this->ftpConnection = $conn_id;
        }
        return $this->ftpConnection;
    }
    /**
     * checkIfFilesExist(): Check if the current list exists on the processed folder
     * @return boolean | exception
     */
    protected function checkIfFilesExist()
    {
        $connection = $this->getConnection();
        $ds = \DIRECTORY_SEPARATOR;
        $files = ftp_nlist($connection, getenv('FTPREMOTEAFTERFOLDER'));
        foreach ($this->fileNames as $fileName) {
            if (!in_array($fileName, $files)) {
                throw new \Exception('File not found!');
            }
        }
        return true;
    }
    protected function uploadTestFiles($qty)
    {
        $ds = \DIRECTORY_SEPARATOR;
        $connection = $this->getConnection();
        for ($i = 0; $i < $qty; $i++) {
            $fileName = uniqid().'.xml';
            $route = getenv('FTPREMOTEFOLDER') . $ds . $fileName;
            ftp_put(
                $connection,
                $route,
                __DIR__.'/TestXML.xml',
                FTP_BINARY
            );
            $this->fileNames[]= $fileName;
        }
    }


    public function testDownload()
    {

        // Stable
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'              => getenv('FTPUSER'),
                    'ftpPassword'          => getenv('FTPPASSWORD'),
                    'ftpServer'            => getenv('FTPSERVER'),
                    'ftpRemoteAfterFolder' => getenv('FTPREMOTEAFTERFOLDER'),
                    'ftpRemoteFolder'      => getenv('FTPREMOTEFOLDER'),
                    'ftpLocalFolder'       => getenv('FTPLOCALFOLDER'),
                    'delete'               => null
                ]
            )
        );
    }

    public function testDownloadWithMissingDir()
    {

        // Stable
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'              => getenv('FTPUSER'),
                    'ftpPassword'          => getenv('FTPPASSWORD'),
                    'ftpServer'            => getenv('FTPSERVER'),
                    'ftpRemoteAfterFolder' => getenv('FTPREMOTEAFTERFOLDER'),
                    'ftpRemoteFolder'      => getenv('FTPREMOTEFOLDER'),
                    'ftpLocalFolder'       => getenv('FTPLOCALFOLDERMISSING'),
                    'delete'               => null
                ]
            )
        );
    }

    public function testIncorrectParameter()
    {
        // Incorrect parameter
        $this->setExpectedException('Exception');
        $this->_ftp->download(
            [
                'ftpUser'         => getenv('FTPUSER'),
                'ftpPassword'     => getenv('FTPPASSWORD'),
                'ftpServer'       => getenv('FTPSERVER'),
                'ftpRemoteAfterFolder' => null,
                'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'),
                'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'),
            ]
        );
    }

    /**
     * @expectedException
     */
    public function testMissingParameter()
    {
        $this->setExpectedException('Exception');
        // Missing Parameter
        $this->_ftp->download(
            [
                'ftpUser'        => getenv('FTPUSER'),
                'ftpPassword'    => getenv('FTPPASSWORD'),
                'ftpLocalFolder' => getenv('FTPSERVER'),
                'delete'         => null
            ]
        );

        // $this->fail("An expected exception for a missing parameter has not been rised");
    }

    /**
     * @expectedException
     */
    public function testMove()
    {
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'         => getenv('FTPUSER'),
                    'ftpPassword'     => getenv('FTPPASSWORD'),
                    'ftpServer'       => getenv('FTPSERVER'),
                    'ftpRemoteAfterFolder' => getenv('FTPREMOTEAFTERFOLDER'),
                    'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'),
                    'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'),
                    'move'            => 1

                ]
            )
        );
        $this->assertTrue($this->checkIfFilesExist());
    }

    /**
     * @expectedException
     */
    public function testDelete()
    {
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'         => getenv('FTPUSER'),
                    'ftpPassword'     => getenv('FTPPASSWORD'),
                    'ftpServer'       => getenv('FTPSERVER'),
                    'ftpRemoteAfterFolder' => getenv('FTPREMOTEAFTERFOLDER'),
                    'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'),
                    'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'),
                    'delete'          => 1
                ]
            )
        );
    }
}
