<?php
/**
 * File: FTPDownloaderTest.php
 *
 * @author mrx, Dmitriy, Etor
 *
 * @package    DataImportFTPDownloader
 * @subpackage FTPDownloader
 * @version    1.0.3
 */
namespace DataImportFTPDownloader;

/**
 * Class FTPDownloader
 *
 * @package:   DataImportFTPDownloader
 * @subpackage FTPDownloader
 * @version:   1.0.3
 */
class FTPDownloader
{

    /**
     * Required fields
     *
     * @var array $config
     */
    private $config = [
        'ftpUser',
        'ftpPassword',
        'ftpServer',
        'ftpRemoteFolder', // Folder where the xml are contained
        'ftpRemoteAfterFolder', // Folder to move the xml after downloaded
        'ftpLocalFolder', // Folder to download the xml files, has to be absolute.
    ];

    /**
     * Config Validation
     *
     * @param array $passedConfig
     *
     * @return void
     * @throws Exception
     */
    private function configValidation(array $passedConfig)
    {
        foreach ($this->config as $value) {
            if (!array_key_exists($value, $passedConfig) || is_null($passedConfig[$value])) {
                throw new \Exception("Error: Missing parameter {$value}");
            }
        }
        $this->config = $passedConfig;

        return;
    }

    /**
     * Download, move, remove files via ftp connection
     *
     * @param array $configftpRemoteAfterFolder
     *
     * @return bool
     */
    public function download(array $config)
    {
        $ds = \DIRECTORY_SEPARATOR;
        $this->configValidation($config);
        $conn_id = ftp_ssl_connect($this->config['ftpServer']) or die("Can't connect " . $this->config['ftpServer']);

        ftp_login($conn_id, $this->config['ftpUser'], $this->config['ftpPassword']);
        ftp_pasv($conn_id, true);

        $res = ftp_nlist($conn_id, $this->config['ftpRemoteFolder']);
        if ($res) {
            $dir = $this->config['ftpLocalFolder'];
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            foreach ($res as $item) {
                $fileName = $dir . $ds . $item;
                // move parameter
                ftp_get($conn_id, $fileName, $this->config['ftpRemoteFolder'] . $ds . $item, FTP_TEXT);
                if (isset($this->config['move']) && (int)$this->config['move'] === 1) {
                    ftp_rename(
                        $conn_id,
                        $this->config['ftpRemoteFolder'] . $ds . $item,
                        $this->config['ftpRemoteAfterFolder'] . $ds . $item
                    );
                }
                // delete parameter
                if (isset($this->config['delete']) && (int)$this->config['delete'] === 1) {
                    ftp_delete($conn_id, $this->config['ftpRemoteFolder'] . $ds . $item);
                }
            }
        }

        return true;
    }
}
